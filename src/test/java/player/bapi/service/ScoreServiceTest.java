package player.bapi.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import player.bapi.component.GameList;
import player.bapi.component.Score;
import player.bapi.component.Step;

class ScoreServiceTest {

    ScoreService scoreService = new ScoreService(new GameList());

    @BeforeEach
    void setUp() {
        scoreService = new ScoreService(new GameList());
    }

    @AfterEach
    void tearDown() {
        scoreService = null;
    }

    @Test
    void addNewStep() {
        scoreService.addNewStep(new Step(1L, Score.STRIKE));
        Assertions.assertEquals(scoreService.getGameList().getHashMap().get(1L).size(), 1);
        scoreService.addNewStep(new Step(1L, Score.ONE));
        Assertions.assertEquals(scoreService.getGameList().getHashMap().get(1L).size(), 2);
        scoreService.addNewStep(new Step(2L, Score.NINE));
        Assertions.assertEquals(scoreService.getGameList().getHashMap().get(2L).size(), 1);
        Assertions.assertEquals(scoreService.getGameList().getHashMap().size(), 2);
    }

    @Test
    void getUserData() {
        scoreService.addNewStep(new Step(1L, Score.ZERO));
        Assertions.assertEquals(scoreService.getUserData(1L).size(), 1);
        scoreService.addNewStep(new Step(1L, Score.STRIKE));
        Assertions.assertEquals(scoreService.getUserData(1L).size(), 1);
        scoreService.addNewStep(new Step(1L, Score.SPARE));
        Assertions.assertEquals(scoreService.getUserData(1L).size(), 1);
        scoreService.addNewStep(new Step(1L, Score.FOUR));
        Assertions.assertEquals(scoreService.getUserData(1L).size(), 2);
    }

    @Test
    void getGameList() {
        Assertions.assertNotNull(scoreService.getGameList());
        scoreService.addNewStep(new Step(1L, Score.NINE));
        Assertions.assertEquals(scoreService.getGameList().getHashMap().size(), 1);
        scoreService.addNewStep(new Step(2L, Score.NINE));
        Assertions.assertEquals(scoreService.getGameList().getHashMap().size(), 2);
    }
}