package player.bapi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import player.bapi.component.Countings;
import player.bapi.component.GameList;
import player.bapi.component.Score;
import player.bapi.component.Step;

@SpringBootTest
class BapiApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    GameList gameList;

    @Test
    public void addNewData() {
        gameList.addNewData(new Step(1L, Score.STRIKE));
        gameList.addNewData(new Step(1L, Score.SEVEN));

        //System.out.println("Show results -> " + new Countings().showResults(1L, gameList.getHashMap()));

        gameList.addNewData(new Step(1L, Score.SPARE));
        gameList.addNewData(new Step(1L, Score.SEVEN));
        gameList.addNewData(new Step(1L, Score.TWO));


        gameList.addNewData(new Step(1L, Score.NINE));
        gameList.addNewData(new Step(1L, Score.SPARE));
        gameList.addNewData(new Step(1L, Score.STRIKE));
        gameList.addNewData(new Step(1L, Score.STRIKE));
        gameList.addNewData(new Step(1L, Score.STRIKE));
        gameList.addNewData(new Step(1L, Score.TWO));
        gameList.addNewData(new Step(1L, Score.THREE));
        gameList.addNewData(new Step(1L, Score.SIX));
        gameList.addNewData(new Step(1L, Score.SPARE));


        //gameList.addNewData(new Step(1L, Score.STRIKE));
        gameList.addNewData(new Step(1L, Score.SEVEN));
        gameList.addNewData(new Step(1L, Score.SPARE));
        //gameList.addNewData(new Step(1L, Score.ZERO));
        //gameList.addNewData(new Step(1L, Score.STRIKE));
        gameList.addNewData(new Step(1L, Score.THREE));
        //gameList.addNewData(new Step(1L, Score.SPARE));
        //gameList.addNewData(new Step(1L, Score.FOUR));

        //System.out.println("Countings -> " + new Countings().countNow(1L, gameList.getHashMap()));
        Assertions.assertEquals(new Countings().countNow(1L, gameList.getHashMap()), 168);
    }

    @Test
    public void showResults() {
        gameList.addNewData(new Step(1L, Score.SPARE));
        gameList.addNewData(new Step(1L, Score.SEVEN));
        gameList.addNewData(new Step(1L, Score.TWO));
        //System.out.println("Showing results -> " + new Countings().showResults(1L, gameList.getHashMap()));
        Assertions.assertNotNull(new Countings().showResults(1L, gameList.getHashMap()));
    }
}
