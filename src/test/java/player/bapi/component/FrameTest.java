package player.bapi.component;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Stack;

class FrameTest {

    GameList gameList;

    @BeforeEach
    void setUp() {
        gameList = new GameList();
    }

    @AfterEach
    void tearDown() {
        gameList = null;
    }

    @Test
    void getStack() {
        gameList.addNewData(new Step(1L, Score.STRIKE));
        Assertions.assertEquals(gameList.getHashMap().get(1L).size(), 1);
        gameList.addNewData(new Step(1L, Score.ONE));
        Assertions.assertEquals(gameList.getHashMap().get(1L).size(), 2);
        gameList.addNewData(new Step(1L, Score.SPARE));
        Assertions.assertEquals(gameList.getHashMap().get(1L).size(), 2);
        gameList.addNewData(new Step(1L, Score.SPARE));
        Assertions.assertEquals(gameList.getHashMap().get(1L).size(), 2);
    }

    @Test
    void isCompleted() {
        gameList.addNewData(new Step(1L, Score.STRIKE));
        Assertions.assertTrue(gameList.getHashMap().get(1L).getLast().isCompleted());
        gameList.addNewData(new Step(1L, Score.ONE));
        Assertions.assertFalse(gameList.getHashMap().get(1L).getLast().isCompleted());
        gameList.addNewData(new Step(1L, Score.STRIKE));
        Assertions.assertFalse(gameList.getHashMap().get(1L).getLast().isCompleted());
        gameList.addNewData(new Step(1L, Score.SPARE));
        Assertions.assertTrue(gameList.getHashMap().get(1L).getLast().isCompleted());
    }

    @Test
    void setStack() {
        Stack<Step> expected = new Stack<>();
        expected.push(new Step(1L, Score.STRIKE));
        gameList.addNewData(new Step(1L, Score.ONE));
        gameList.getHashMap().get(1L).getLast().setStack(expected);
        Assertions.assertEquals(expected, gameList.getHashMap().get(1L).getLast().getStack());
    }

    @Test
    void setCompleted() {
        boolean expected = true;
        gameList.addNewData(new Step(1L, Score.ONE));
        Assertions.assertNotEquals(expected, gameList.getHashMap().get(1L).getLast().isCompleted());
        gameList.getHashMap().get(1L).getLast().setCompleted(expected);
        Assertions.assertEquals(expected, gameList.getHashMap().get(1L).getLast().isCompleted());
    }
}