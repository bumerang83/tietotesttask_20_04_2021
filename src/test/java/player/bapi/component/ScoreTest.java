package player.bapi.component;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ScoreTest {

    Score score;

    @BeforeEach
    void setUp() {
        score = Score.SIX;
    }

    @AfterEach
    void tearDown() {
        score = null;
    }

    @Test
    void getValue() {
        Assertions.assertEquals(score.getValue(), 6);
    }

    @Test
    void getScore() {
        Assertions.assertEquals(score.getScore(), '6');
    }

    @Test
    void values() {
        Assertions.assertEquals(Score.values().length, 12);
    }

    @Test
    void valueOf() {
        Assertions.assertEquals(score.getValue(), 6);
    }
}