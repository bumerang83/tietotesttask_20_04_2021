package player.bapi.component;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GameListTest {

    GameList gameList;

    @BeforeEach
    void setUp() {
        gameList = new GameList();
    }

    @AfterEach
    void tearDown() {
        gameList = null;
    }

    @Test
    void addNewData() {
        Assertions.assertEquals(gameList.getHashMap().size(), 0);
        gameList.addNewData(new Step(1L, Score.STRIKE));
        Assertions.assertEquals(gameList.getHashMap().size(), 1);
        gameList.addNewData(new Step(2L, Score.NINE));
        Assertions.assertEquals(gameList.getHashMap().size(), 2);
        gameList.addNewData(new Step(3L, Score.SPARE));
        Assertions.assertEquals(gameList.getHashMap().size(), 2);
        gameList.addNewData(new Step(3L, Score.ONE));
        Assertions.assertEquals(gameList.getHashMap().size(), 3);
    }

    @Test
    void getHashMap() {
        Assertions.assertNotNull(gameList.getHashMap());
        Assertions.assertEquals(gameList.getHashMap().size(), 0);
        gameList.addNewData(new Step(1L, Score.STRIKE));
        Assertions.assertEquals(gameList.getHashMap().size(), 1);
    }
}