package player.bapi.component;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StepTest {

    Step step;

    @BeforeEach
    void setUp() {
        step = new Step(1L, Score.EIGHT);
    }

    @AfterEach
    void tearDown() {
        step = null;
    }

    @Test
    void getPlayerId() {
        Assertions.assertEquals(step.getPlayerId(), 1L);
    }

    @Test
    void getScore() {
        Assertions.assertEquals(step.getScore(), Score.EIGHT);
    }

    @Test
    void setPlayerId() {
        step.setPlayerId(2L);
        Assertions.assertEquals(step.getPlayerId(), 2L);
    }

    @Test
    void setScore() {
        step.setScore(Score.ONE);
        Assertions.assertEquals(step.getScore(), Score.ONE);
    }
}