package player.bapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BapiApplication.class, args);
    }

}
