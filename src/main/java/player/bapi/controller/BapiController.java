package player.bapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import player.bapi.component.Frame;
import player.bapi.component.Step;
import player.bapi.service.ScoreService;

import java.util.LinkedList;

@RestController
@RequestMapping(path = "bapi/v1")
public class BapiController {

    private final ScoreService scoreService;

    @Autowired
    public BapiController(ScoreService scoreService) {
        this.scoreService = scoreService;
    }

    @PostMapping
    public void addNewStep(@RequestBody Step step) {
        this.scoreService.addNewStep(step);
    }

    @GetMapping("{playerId}")
    LinkedList<Frame> userData(@PathVariable("playerId") Long playerId) {
        return this.scoreService.getUserData(playerId);
    }
}
