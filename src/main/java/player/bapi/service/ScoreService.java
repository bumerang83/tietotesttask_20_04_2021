package player.bapi.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import player.bapi.component.Frame;
import player.bapi.component.GameList;
import player.bapi.component.Step;

import java.util.LinkedList;

@Service
@Getter
@Setter
@NoArgsConstructor
public class ScoreService {
    private GameList gameList;

    @Autowired
    public ScoreService(GameList gameList) {
        this.gameList = gameList;
    }

    public void addNewStep(Step step) {
        this.gameList.addNewData(step);
    }

    public LinkedList<Frame> getUserData(Long playerId) {
        return this.gameList.getHashMap().get(playerId);
    }
}
