package player.bapi.component;

import lombok.Getter;

@Getter
public enum Score {
    ZERO('0'),
    ONE('1'),
    TWO('2'),
    THREE('3'),
    FOUR('4'),
    FIVE('5'),
    SIX('6'),
    SEVEN('7'),
    EIGHT('8'),
    NINE('9'),
    STRIKE('X'),
    SPARE('/');

    private final char score;

    Score(char score) {
        this.score = score;
    }

    public Integer getValue() {
        if (this.score == 'X' || this.score == '/') {
            return Integer.parseInt("10");
        }
        return Integer.parseInt(String.valueOf(score));
    }
}