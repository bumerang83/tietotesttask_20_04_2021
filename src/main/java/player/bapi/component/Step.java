package player.bapi.component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Step {
    private Long playerId;
    private Score score;

    @Override
    public String toString() {
        return "Player Id " + playerId + " Score " + score;
    }
}
