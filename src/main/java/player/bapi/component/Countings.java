package player.bapi.component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

public class Countings {

    public int countNow(Long playerId, HashMap<Long, LinkedList<Frame>> hashMap) {
        LinkedList<Frame> linkedList = hashMap.get(playerId);
        int listSize = linkedList.size();
        int numberNow = 0;
        for (int i = 0; i < listSize; i++) {
            if (i == 10) {
                break;
            }
            numberNow += countStack(linkedList.get(i));
            if (linkedList.get(i).getStack().peek().getScore().equals(Score.SPARE)) {
                if (listSize > (i + 1)) {
                    numberNow += countSpareStack(linkedList.get(i + 1));
                }
            } else if (linkedList.get(i).getStack().peek().getScore().equals(Score.STRIKE)) {
                if (listSize > (i + 2)) {
                    if (linkedList.get(i + 1).getStack().size() == 2) {
                        numberNow += countStrikeStack(linkedList.get(i + 1));
                    } else {
                        numberNow += 10;
                        numberNow += countSpareStack(linkedList.get(i + 2));
                    }
                }
            }
        }
        return numberNow;
    }

    public LinkedList<Frame> showResults(Long playerId, HashMap<Long, LinkedList<Frame>> hashMap) {
        return hashMap.get(playerId);
    }

    private int countStack(Frame frame) {
        if (frame.isCompleted()) {
            Stack<Step> stack = frame.getStack();
            if (stack.peek().getScore().equals(Score.SPARE) || stack.peek().getScore().equals(Score.STRIKE)) {
                return 10;
            } else {
                int sum = 0;
                Iterator<Step> iterator = stack.iterator();
                while (iterator.hasNext()) {
                    sum += iterator.next().getScore().getValue();
                }
                return sum;
            }
        } else return 0;
    }

    private int countSpareStack(Frame frame) {
        if (frame.isCompleted()) {
            Stack<Step> stack = frame.getStack();
            if (stack.peek().getScore().equals(Score.STRIKE)) {
                return 10;
            } else {
                return stack.elementAt(0).getScore().getValue();
            }
        } else return 0;
    }

    private int countStrikeStack(Frame frame) {
        if (frame.isCompleted()) {
            Stack<Step> stack = frame.getStack();
            if (stack.peek().getScore().equals(Score.SPARE)) {
                return 10;
            } else {
                int sum = 0;
                Iterator<Step> iterator = stack.iterator();
                while (iterator.hasNext()) {
                    sum += iterator.next().getScore().getValue();
                }
                return sum;
            }
        } else return 0;
    }
}
