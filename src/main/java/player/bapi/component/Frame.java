package player.bapi.component;

import lombok.Getter;
import lombok.Setter;

import java.util.Stack;

@Getter
@Setter
public class Frame {
    private Stack<Step> stack;
    private boolean completed = false;

    public Frame(Stack<Step> stack) {
        this.stack = stack;
    }
}
