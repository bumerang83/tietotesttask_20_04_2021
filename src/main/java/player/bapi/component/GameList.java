package player.bapi.component;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;

@Component
@Getter
public class GameList {
    private HashMap<Long, LinkedList<Frame>> hashMap;

    public GameList() {
        this.hashMap = new HashMap<>();
    }

    public boolean addNewData(Step step) {
        if(!this.hashMap.containsKey(step.getPlayerId())) {
            if(controlNotSpare(step)) {
                this.hashMap.put(step.getPlayerId(), new LinkedList<>());
                addNewFrame(step);
                return true;
            }
        }
        else {
            if(!gameCompleted(step.getPlayerId())) {
                if(frameCompleted(step)) {
                    addNewFrame(step);
                    return true;
                }
                else {
                    if(!gameCompleted(step.getPlayerId())) {
                        continueFrame(step);
                        return true;
                    }
                }
            }
            else if(extraSpare(step.getPlayerId())) {
                if(!step.getScore().equals(Score.SPARE)) {
                    addNewFrame(step);
                    this.hashMap.get(step.getPlayerId()).getLast().setCompleted(true);
                    return true;
                }
            }
            else if(extraStrike(step.getPlayerId())) {
                if(!step.getScore().equals(Score.SPARE)) {
                    addNewFrame(step);
                    this.hashMap.get(step.getPlayerId()).getLast().setCompleted(true);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean extraSpare(Long playerId) {
        LinkedList<Frame> linkedList = this.getHashMap().get(playerId);
        if(linkedList.get(9).getStack().peek().getScore().equals(Score.SPARE)) {
            return true;
        }
        return false;
    }

    private boolean extraStrike(Long playerId) {
        LinkedList<Frame> linkedList = this.getHashMap().get(playerId);
        if(linkedList.get(9).getStack().peek().getScore().equals(Score.STRIKE) && linkedList.size() != 12) {
            return true;
        }
        return false;
    }

    private boolean gameCompleted(Long playerId) {
        LinkedList<Frame> linkedList = this.getHashMap().get(playerId);
        if(linkedList.size() >= 10) {
            if(linkedList.getLast().isCompleted()) {
                return true;
            }
        }
        return false;
    }

    private boolean addNewFrame(Step step) {
        Frame frame = new Frame(new Stack<>());
        if(step.getScore() == Score.STRIKE) {
            frame.getStack().push(step);
            frame.setCompleted(true);
            this.getHashMap().get(step.getPlayerId()).add(frame);
            return true;
        }
        else if(controlNotSpare(step)) {
            frame.getStack().push(step);
            this.getHashMap().get(step.getPlayerId()).add(frame);
            return true;
        }
        return false;
    }

    private boolean continueFrame(Step step) {
        if(step.getScore() == Score.STRIKE) {
            return false;
        }
        Frame frame = this.getHashMap().get(step.getPlayerId()).getLast();
        if(step.getScore() == Score.SPARE) {
            frame.getStack().push(step);
            frame.setCompleted(true);
            return true;
        }
        if(frame.getStack().peek().getScore().getValue() + step.getScore().getValue() <= 10) {
            if(frame.getStack().peek().getScore().getValue() + step.getScore().getValue() == 10) {
                step.setScore(Score.SPARE);
            }
            frame.getStack().push(step);
            frame.setCompleted(true);
            return true;
        }
        return false;
    }

    private boolean lastFrame(Step step) {
        Frame frame = this.getHashMap().get(step.getPlayerId()).getLast();
        if(frame.getStack().peek().equals(Score.SPARE)) {
            if(controlNotSpare(step)) {
                frame.getStack().push(step);
                frame.setCompleted(true);
                return true;
            }
        }
        if(frame.getStack().size() == 1 && frame.getStack().peek().equals(Score.STRIKE)) {
            if(controlNotSpare(step) ) {
                frame.getStack().push(step);
                frame.setCompleted(true);
                return true;
            }
        }
        return false;
    }

    private boolean controlNotSpare(Step step) {
        if(step.getScore() == Score.SPARE) {
            return false;
        }
        else {
            return true;
        }
    }

    private boolean frameCompleted(Step step) {
        return this.getHashMap().get(step.getPlayerId()).getLast().isCompleted();
    }
}
